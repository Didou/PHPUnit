<?php

/**
 * Class UserTest
 *
 * @author                 Didier Youn <didier.youn@gmail.com>
 * @copyright              ESGI
 * @license                http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
require_once __DIR__ . '/vendor/autoload.php';
require 'Product.php';
require 'User.php';

class ProductTest extends \PHPUnit\Framework\TestCase
{
    /** @var Product $product */
    protected $product = null;
    /** @var User $user */
    protected $user = null;

    /**
     * Launch before each test methods
     */
    protected function setUp()
    {
        if (is_null($this->product)) {
            $this->user = new User('Did', 'Test', 'did.94350@gmail.com', 21);
            $this->product = new Product('Dior', Product::PRODUCT_ACTIVE, $this->user);
        }
    }

    /**
     * Is not valid product name
     */
    public function testIsNotValidProductName()
    {
        $this->product->setName(135);
        $this->assertEquals($this->product->isValid($this->product), false);
    }

    /**
     * Product is not active
     */
    public function testIsNotProductActive()
    {
        $this->product->setState(Product::PRODUCT_NOT_ACTIVE);
        $this->assertEquals($this->product->isValid($this->product), false);
    }

    /**
     * Product owner is wrong
     */
    public function testHasNotProductOwner()
    {
        $this->product->setOwner(null);
        $this->assertEquals($this->product->isValid($this->product), false);
    }

    /**
     * Product owner is wrong
     */
    public function testWrongDataAsProductOwner()
    {
        $this->product->setOwner("loool");
        $this->assertEquals($this->product->isValid($this->product), false);
    }

    /**
     * Launch after each test methods
     */
    protected function tearDown()
    {
        //$this->product = null;
    }
}