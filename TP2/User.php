<?php

/**
 * Class User
 *
 * @author                 Didier Youn <didier.youn@gmail.com>
 * @copyright              ESGI
 * @license                http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class User
{
    /** @var string $email */
    private $email;
    /** @var string $nom */
    private $nom;
    /** @var string $prenom */
    private $prenom;
    /** @var string $age */
    private $age;

    /**
     * User constructor.
     *
     * @param $nom
     * @param $prenom
     * @param $email
     * @param $age
     */
    public function __construct($nom, $prenom, $email, $age)
    {
        $this->nom      = $nom;
        $this->prenom   = $prenom;
        $this->email    = $email;
        $this->age      = $age;
    }

    /**
     * Check if current user is valid
     *
     * @param User $user
     * @return bool
     */
    public function isValid($user)
    {
        if ($this->_isValidName($user) && $this->_isValidLastname($user)
        && $this->_isValidEmail($user) && $this->_isValidAge($user)) {
            return true;
        }

        return false;
    }

    /**
     * Check if valid name
     *
     * @param $user
     * @return bool
     */
    protected function _isValidName($user)
    {
        if (!isset($user->nom)) {
            return false;
        }
        if (!preg_match("/^[a-zA-Z ]*$/", $user->nom)) {
            return false;
        }

        return true;
    }

    /**
     * Check if valid lastname
     *
     * @param $user
     * @return bool
     */
    protected function _isValidLastname($user)
    {
        if (!isset($user->prenom)) {
            return false;
        }
        if (!preg_match("/^[a-zA-Z ]*$/", $user->prenom)) {
            return false;
        }

        return true;
    }

    /**
     * Check if valid email
     *
     * @param User $user
     * @return bool
     */
    protected function _isValidEmail($user)
    {
        if (!isset($user->email)) {
            return false;
        }
        /** @var string $mail */
        $mail = $user->email;
        if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        return true;
    }

    /**
     * Check if valid age
     * --> $age > 13
     *
     * @param User $user
     * @return bool
     */
    protected function _isValidAge($user)
    {
        if (!isset($user->age) || !is_numeric($user->age)) {
            return false;
        }
        /** @var int $age */
        $age = (int)$user->age;
        if ($age >= 13) {
            return true;
        }

        return false;
    }

    /**
     * @param string $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }
}