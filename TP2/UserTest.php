<?php

/**
 * Class UserTest
 *
 * @author                 Didier Youn <didier.youn@gmail.com>
 * @copyright              ESGI
 * @license                http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
require_once __DIR__ . '/vendor/autoload.php';
require 'User.php';

class UserTest extends \PHPUnit\Framework\TestCase
{
    /** @var null $user */
    protected $user = null;

    public function setUp()
    {
        if (is_null($this->user)) {
            /** @var User user */
           $this->user = new User('Test', 'Test', 'test@gmail.com', 21);
        }
    }

    /**
     * @covers : User::isValid()
     */
    public function testIsValid()
    {
        /** @var User $user */
        $user = new User('Youn', 'Didier', 'did.94350@gmail.com', '21');
        $this->assertEquals($user->isValid($user), true);
    }

    /**
     * Name/lastname not valid
     */
    public function testIsNotAttributeNameNotValid()
    {
        /** @var User $user */
        $user = new User(1555, 1, 'loool', '21');
        $this->assertEquals($user->isValid($user), false);
    }

    /**
     * Email not valid
     */
    public function testIsNotEmailValid()
    {
        /** @var User $user */
        $user = new User('Intha Amouany', 'Marc', 'loool', '21');
        $this->assertEquals($user->isValid($user), false);
    }

    /**
     * Not valid age
     */
    public function testIsNotValidAge()
    {
        /** @var User $user */
        $user = new User('Renault', 'Antoine', 'antoine@gmail.com', '12');
        $this->assertEquals($user->isValid($user), false);
    }

    /**
     * Not valid age with string
     */
    public function testIsNotValidAgeByString()
    {
        /** @var User $user */
        $user = new User('Renault', 'Antoine', 'antoine@gmail.com', 'mdr');
        $this->assertEquals($user->isValid($user), false);
    }

    /**
     * Valid age
     */
    public function testIsValidAge()
    {
        /** @var User $user */
        $user = new User('Monjaret', 'Vincent', 'vincent@gmail.com', '21');
        $this->assertEquals($user->isValid($user), true);
    }
}