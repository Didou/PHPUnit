<?php

class CalculatriceException
{

    public function throwException($message = null)
    {
        throw new Exception($message);
    }
}