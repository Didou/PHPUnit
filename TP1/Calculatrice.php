<?php

require('CalculatriceException.php');

class Calculatrice
{
    /**
     * @param $a
     * @param $b
     *
     * @return bool | int
     * @throws Exception
     */
    public function add($a, $b)
    {
        if (!isset($a) || !isset($b)) {
            return false;
        }

        if (!$this->checkIfIsNumber($a) || !$this->checkIfIsNumber($b)) {
            $this->throwException('Undefined digit value');
        }

        return $a + $b;
    }

    /**
     * @param $a
     * @param $b
     *
     * @return int
     */
    public function sub($a, $b)
    {
        return $a - $b;
    }

    /**
     * @param $a
     * @param $b
     *
     * @return float|int
     */
    public function division($a, $b)
    {
        if (!isset($a) || !isset($b)) {
            return false;
        }

        if (!$this->checkIfIsNumber($a) || !$this->checkIfIsNumber($b)) {
            $this->throwException('Undefined digit value');
        }

        if ($b == 0) {
            $this->throwException('Cant make a division by zero');
        }

        return $a / $b;
    }

    public function multiplicate($a, $b)
    {
        return $a * $b;
    }

    /**
     * @param string $string
     *
     * @throws Exception
     */
    public function throwException($string = null)
    {
        /** @var CalculatriceException $calculatriceException */
        $calculatriceException = new CalculatriceException();
        $calculatriceException->throwException($string);
    }

    /**
     * @param $number
     * @return bool
     */
    private function checkIfIsNumber($number)
    {
        if(!is_numeric($number)) {
            return false;
        }

        return true;
    }
}