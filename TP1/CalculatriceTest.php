<?php

require_once __DIR__ . '/vendor/autoload.php';
require 'Calculatrice.php';

class CalculatriceTest extends \PHPUnit\Framework\TestCase
{
    public static function setUpBeforeClass()
    {
        /** @var Calculatrice $calculatrice */
        $calculatrice = new Calculatrice();
    }

    /**
     * @covers Calculatrice::add
     */
    function testAdd()
    {
        /** @var Calculatrice $calculatrice */
        $calculatrice = new Calculatrice();
        /** @var int $add */
        $add = $calculatrice->add(2, 2);

        $this->assertEquals($add, 4);
    }

    /**
     * Test Calculatrice()->add()
     *
     * @expectedException Exception
     */
    function testAddWithoutNumbers()
    {
        /** @var Calculatrice $calculatrice */
        $calculatrice = new Calculatrice();
        /** @var int $add */
        $add = $calculatrice->add('Didier', 'Youn');

        $this->assertFalse($add, 4);
    }

    /**
     * Test Calculatrice()->sub()
     */
    function testSub()
    {
        /** @var Calculatrice $calculatrice */
        $calculatrice = new Calculatrice();
        /** @var int $sub */
        $sub = $calculatrice->sub(5, 3);

        $this->assertEquals($sub, 2);
    }

    /**
     * Test Calculatrice()->division()
     */
    function testDivision()
    {
        /** @var Calculatrice $calculatrice */
        $calculatrice = new Calculatrice();
        /** @var int $reste */
        $reste = $calculatrice->division(9, 3);

        $this->assertEquals($reste, 3);
    }

    /**
     * Test Calculatrice()->division()
     *
     * @expectedException Exception
     */
    function testDivisionByZero()
    {
        /** @var Calculatrice $calculatrice */
        $calculatrice = new Calculatrice();
        /** @var int $reste */
        $reste = $calculatrice->division(9, 0);

        $this->assertEquals($reste, 0);
    }

    function testMultiplication()
    {
        $calculatrice = new Calculatrice();
        $response = $calculatrice->multiplicate(2, 2);

        $this->assertLessThanOrEqual($response, 4);
        $this->assertLessThanOrEqual($response, 2);
    }

    /**
     * @expectedException Exception
     */
    function testException()
    {
        /** @var Calculatrice $calculatrice */
        $calculatrice = new Calculatrice();

        $calculatrice->throwException();
    }

}