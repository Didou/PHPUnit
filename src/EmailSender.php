<?php

/**
 * Class EmailSender
 *
 * @author              Didier Youn <didier.youn@gmail.com>
 * @copyright           Copyright (c) 2016
 * @license             http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link                http://www.didier-youn.com
 */
class EmailSender
{
    public function __construct()
    {
    }

    /**
     * Send Email
     *
     * @param $emailReceiver
     * @param $content
     * @throws Exception
     */
    public function sendEmail($emailReceiver, $content)
    {
        throw new Exception('Not implemented');
    }
}