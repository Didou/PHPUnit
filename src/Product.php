<?php

/**
 * Class Product
 *
 * @author              Didier Youn <didier.youn@gmail.com>
 * @copyright           Copyright (c) 2016
 * @license             http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link                http://www.didier-youn.com
 */
class Product
{
    /**
     * @string PRODUCT_ACTIVE
     */
    const PRODUCT_ACTIVE = "ACTIVE";
    /**
     * @string PRODUCT_NOT_ACTIVE
     */
    const PRODUCT_NOT_ACTIVE = "NOT_ACTIVE";
    /** @var string $name */
    private $name;
    /** @var string $state */
    private $state;
    /** @var string $owner */
    private $owner;

    /**
     * Product constructor.
     *
     * @param $name
     * @param $state
     * @param $owner
     */
    public function __construct($name, $state, $owner)
    {
        $this->name = $name;
        $this->state = $state;
        $this->owner = $owner;
    }

    /**
     * Check if product is valid
     *
     * @param Product $product
     * @return bool
     */
    public function isValid(Product $product)
    {
        if ($this->_checkProductName($product->name) 
            && $this->_checkProductState($product->state)
            && $this->_checkProductOwner($product->owner))
        {
            return true;
        }

        return false;
    }

    /**
     * Check product name
     *
     * @param string $productName
     * @return bool
     */
    protected function _checkProductName($productName)
    {
        if (!isset($productName) || !preg_match("/^[a-zA-Z ]*$/", $productName)) {
            return false;
        }

        return true;
    }

    /**
     * Check product state (active/not active)
     *
     * @param string $state
     * @return bool
     */
    protected function _checkProductState($state)
    {
        if (!isset($state) || $state != self::PRODUCT_ACTIVE) {
            return false;
        }

        return true;
    }

    /**
     * Check product owner
     *
     * @param $owner
     * @return bool
     */
    protected function _checkProductOwner($owner)
    {
        if (!isset($owner)) {
            return false;
        }
        if ($owner instanceof User && $owner->isValid($owner)) {
            return true;
        }

        return false;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }
}