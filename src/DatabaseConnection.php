<?php

/**
 * Class DatabaseConnection
 *
 * @author              Didier Youn <didier.youn@gmail.com>
 * @copyright           Copyright (c)
 * @license             http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link                http://www.didier-youn.com
 */
class DatabaseConnection
{
    /**
     * DatabaseConnection constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param Exchange $exchange
     * @throws Exception
     */
    public function saveExchange($exchange)
    {
        throw new Exception('Not implemented');
    }
}