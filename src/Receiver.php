<?php

/**
 * Class Receiver
 *
 * @author              Didier Youn <didier.youn@gmail.com>
 * @copyright           Copyright (c) 2016
 * @license             http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link                http://www.didier-youn.com
 */
class Receiver
{
    /** @var string $email */
    private $email;
    /** @var string $nom */
    private $nom;
    /** @var string $prenom */
    private $prenom;
    /** @var string $age */
    private $age;

    /**
     * Receiver constructor.
     *
     * @param $nom
     * @param $prenom
     * @param $email
     * @param $age
     */
    public function __construct($nom, $prenom, $email, $age)
    {
        $this->nom      = $nom;
        $this->prenom   = $prenom;
        $this->email    = $email;
        $this->age      = $age;
    }

    /**
     * Check if current receiver is valid
     *
     * @param receiver $receiver
     * @return bool
     */
    public function isValid($receiver)
    {
        if ($this->_isValidName($receiver) && $this->_isValidLastname($receiver)
            && $this->_isValidEmail($receiver) && $this->_isValidAge($receiver)) {
            return true;
        }

        return false;
    }

    /**
     * Check if valid name
     *
     * @param $receiver
     * @return bool
     */
    protected function _isValidName($receiver)
    {
        if (!isset($receiver->nom)) {
            return false;
        }
        if (!preg_match("/^[a-zA-Z ]*$/", $receiver->nom)) {
            return false;
        }

        return true;
    }

    /**
     * Check if valid lastname
     *
     * @param $receiver
     * @return bool
     */
    protected function _isValidLastname($receiver)
    {
        if (!isset($receiver->prenom)) {
            return false;
        }
        if (!preg_match("/^[a-zA-Z ]*$/", $receiver->prenom)) {
            return false;
        }

        return true;
    }

    /**
     * Check if valid email
     *
     * @param $receiver
     * @return bool
     */
    protected function _isValidEmail($receiver)
    {
        if (!isset($receiver->email)) {
            return false;
        }
        /** @var string $mail */
        $mail = $receiver->email;
        if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        return true;
    }

    /**
     * Check if valid age
     * --> $age > 13
     *
     * @param $receiver
     * @return bool
     */
    protected function _isValidAge($receiver)
    {
        if (!isset($receiver->age) || !is_numeric($receiver->age)) {
            return false;
        }
        /** @var int $age */
        $age = (int)$receiver->age;
        if ($age >= 13) {
            return true;
        }

        return false;
    }

    /**
     * @param string $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return string
     */
    public function getAge()
    {
        return $this->age;
    }
}