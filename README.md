# PHPUnit - Didier YOUN

**Introduction**
> TP sur les tests unitaires destiné à __Mr. Sébastien Souphron__.<br/>
> Les tests ont été implémenté en PHP et la librairie choisit est [PHPUnit](https://phpunit.de/)

**Requirements**
* Composer

**Usage**
```
composer install
```
**Roadmap**
```
> cd ROOT_FOOLDER
> vendor/bin/phpunit ExchangeTest.php
```