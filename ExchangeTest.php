<?php

/**
 * Class ExchangeTest
 *
 * @author              Didier Youn <didier.youn@gmail.com>
 * @copyright           Copyright (c) 2016
 * @license             http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link                http://www.didier-youn.com
 */
require_once __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/src/Receiver.php';
require __DIR__ . '/src/DatabaseConnection.php';
require __DIR__ . '/src/Product.php';
require __DIR__ . '/src/EmailSender.php';

require 'Exchange.php';

class ExchangeTest extends \PHPUnit\Framework\TestCase
{
    /** @var Exchange $exchange */
    protected $exchange = null;

    /**
     * Launch before each test methods
     */
    protected function setUp()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Receiver $receiverMock */
        $receiverMock = $this->createMock(Receiver::class);
        $receiverMock->method('isValid')
            ->willReturn(true);
        $receiverMock->method('getAge')
            ->willReturn(21);
        /** @var PHPUnit_Framework_MockObject_MockObject|Product $productMock */
        $productMock = $this->createMock(Product::class);
        $productMock->method('isValid')
            ->willReturn(true);
        /** @var PHPUnit_Framework_MockObject_MockObject|EmailSender $emailSender */
        $emailSender = $this->createMock(EmailSender::class);
        $emailSender->method('sendEmail')
            ->willReturn(true);
        /** @var string $dateDebut */
        $dateDebut = "01/01/2016";
        /** @var string $dateFin */
        $dateFin = "01/01/2017";
        /** @var PHPUnit_Framework_MockObject_MockObject|DatabaseConnection $dbConnection */
        $dbConnection = $this->createMock(DatabaseConnection::class);
        $dbConnection->method('saveExchange')
            ->willReturn(true);
        if (is_null($this->exchange)) {
            $this->exchange = new Exchange(
                $receiverMock,
                $productMock,
                $emailSender,
                $dateDebut,
                $dateFin,
                $dbConnection
            );
        }
    }

    /**
     * @covers Exchange::save()
     */
    public function testTrySaveExchangeWithProperData()
    {
        /** @var Exchange $exchange */
        $exchange = $this->exchange;
        $this->assertEquals($exchange->save($exchange), true);
    }

    /**
     * This test is stupid
     *
     * @covers Exchange::save()
     */
    public function testTrySaveExchangeWitFailedData()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Receiver $receiverMock */
        $receiverMock = $this->createMock(Receiver::class);
        $receiverMock->method('isValid')
            ->willReturn(false);
        /** @var PHPUnit_Framework_MockObject_MockObject|Product $productMock */
        $productMock = $this->createMock(Receiver::class);
        $productMock->method('isValid')
            ->willReturn(false);
        /** @var PHPUnit_Framework_MockObject_MockObject|EmailSender $sendMailerMock */
        $sendMailerMock = $this->createMock(EmailSender::class);
        $sendMailerMock->method('sendEmail')
            ->willReturn(false);
        /** @var PHPUnit_Framework_MockObject_MockObject|DatabaseConnection $dbConnection */
        $dbConnection = $this->createMock(DatabaseConnection::class);
        $dbConnection->method('saveExchange')
            ->willReturn(false);
        /** @var Exchange $exchange */
        $exchange = $this->exchange;
        $exchange->setDbConnection($dbConnection);
        $exchange->setProduct($productMock);
        $exchange->setReceiver($receiverMock);
        $exchange->setEmailSender($sendMailerMock);

        $this->assertEquals($exchange->save($exchange), false);
    }

    /**
     * @covers Exchange::save()
     */
    public function testTrySaveExchangeWithWrongDateFormat()
    {
        /** @var Exchange $exchange */
        $exchange = $this->exchange;
        $exchange->setDateDebut('aaaa');
        $exchange->setDateFin('aaaa');

        $this->assertEquals($exchange->save($exchange), false);
    }

    /**
     * @covers Exchange::save()
     */
    public function testTrySaveExchangeWithDateDebutGreaterThanDateFin()
    {
        /** @var Exchange $exchange */
        $exchange = $this->exchange;
        $exchange->setDateDebut('01/01/2018');

        $this->assertEquals($exchange->save($exchange), false);
    }

    /**
     * @covers Exchange::save()
     */
    public function testTrySaveExchangeWithDateFinSmallerThanDateDebut()
    {
        /** @var Exchange $exchange */
        $exchange = $this->exchange;
        $exchange->setDateFin('01/01/2015');

        $this->assertEquals($exchange->save($exchange), false);
    }

    /**
     * @covers Exchange::save()
     */
    public function testTrySaveExchangeWithWrongProduct()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Product $productMock */
        $productMock = $this->createMock(Product::class);
        $productMock->method('isValid')
            ->willReturn(false);
        /** @var Exchange $exchange */
        $exchange = $this->exchange;
        $exchange->setProduct($productMock);

        $this->assertEquals($exchange->save($exchange), false);
    }

    /**
     * @covers Exchange::save()
     */
    public function testTrySaveExchangeWithValidProduct()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Product $productMock */
        $productMock = $this->createMock(Product::class);
        $productMock->method('isValid')
            ->willReturn(true);
        /** @var Exchange $exchange */
        $exchange = $this->exchange;
        $exchange->setProduct($productMock);

        $this->assertEquals($exchange->save($exchange), true);
    }

    /**
     * @covers Exchange::save()
     */
    public function testTrySaveExchangeWithValidReceiver()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Receiver $receiverMock*/
        $receiverMock = $this->createMock(Receiver::class);
        $receiverMock->method('isValid')
            ->willReturn(true);
        $receiverMock->method('getAge')
            ->willReturn(21);
        /** @var Exchange $exchange */
        $exchange = $this->exchange;
        $exchange->setReceiver($receiverMock);

        $this->assertEquals($exchange->save($exchange), true);
    }

    /**
     * @covers Exchange::save()
     */
    public function testTrySaveExchangeWithNotValidReceiver()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Receiver $receiverMock*/
        $receiverMock = $this->createMock(Receiver::class);
        $receiverMock->method('isValid')
            ->willReturn(false);
        /** @var Exchange $exchange */
        $exchange = $this->exchange;
        $exchange->setReceiver($receiverMock);

        $this->assertEquals($exchange->save($exchange), false);
    }

    /**
 * @covers Exchange::save()
 */
    public function testTrySaveExchangeWithValidSendingMail()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|EmailSender $sendMailerMock */
        $sendMailerMock = $this->createMock(EmailSender::class);
        $sendMailerMock->method('sendEmail')
            ->willReturn(true);
        /** @var Exchange $exchange */
        $exchange = $this->exchange;
        $exchange->setEmailSender($sendMailerMock);

        $this->assertEquals($exchange->save($exchange), true);
    }


    /**
     * @covers Exchange::save()
     */
    public function testTrySaveExchangeWithWrongParamInSendingMailMethod()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|EmailSender $sendMailerMock */
        $sendMailerMock = $this->createMock(EmailSender::class);
        $sendMailerMock->method('sendEmail')
            ->willReturn(false);
        /** @var Exchange $exchange */
        $exchange = $this->exchange;
        $exchange->setEmailSender($sendMailerMock);

        $this->assertEquals($exchange->save($exchange), false);
    }

    /**
 * @covers Exchange::save()
 */
    public function testTrySaveExchangeWithValidSendingMailButNotValidReceiver()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|EmailSender $sendMailerMock */
        $sendMailerMock = $this->createMock(EmailSender::class);
        $sendMailerMock->method('sendEmail')
            ->willReturn(true);
        /** @var PHPUnit_Framework_MockObject_MockObject|Receiver $receiverMock */
        $receiverMock = $this->createMock(Receiver::class);
        $receiverMock->method('isValid')
            ->willReturn(false);
        /** @var Exchange $exchange */
        $exchange = $this->exchange;
        $exchange->setEmailSender($sendMailerMock);
        $exchange->setReceiver($receiverMock);

        $this->assertEquals($exchange->save($exchange), false);
    }

    /**
     * @covers Exchange::save()
     */
    public function testTrySaveExchangeWithValidSendingMailButMinorReceiver()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|EmailSender $sendMailerMock */
        $sendMailerMock = $this->createMock(EmailSender::class);
        $sendMailerMock->method('sendEmail')
            ->willReturn(true);
        /** @var PHPUnit_Framework_MockObject_MockObject|Receiver $receiverMock */
        $receiverMock = $this->createMock(Receiver::class);
        $receiverMock->method('getAge')
            ->willReturn(16);
        /** @var Exchange $exchange */
        $exchange = $this->exchange;
        $exchange->setEmailSender($sendMailerMock);
        $exchange->setReceiver($receiverMock);

        $this->assertEquals($exchange->save($exchange), false);
    }

    /**
     * @covers Exchange::save()
     */
    public function testTrySaveExchangeWithValidDbConnection()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|DatabaseConnection $dbConnection */
        $dbConnection = $this->createMock(DatabaseConnection::class);
        $dbConnection->method('saveExchange')
            ->willReturn(true);
        /** @var Exchange $exchange */
        $exchange = $this->exchange;
        $exchange->setDbConnection($dbConnection);

        $this->assertEquals($exchange->save($exchange), true);
    }

    /**
     * @covers Exchange::save()
     * @expectedException Exception
     */
    public function testTrySaveExchangeWithErrorInDbConnectionSave()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|DatabaseConnection $dbConnection */
        $dbConnection = $this->createMock(DatabaseConnection::class);
        $dbConnection->method('saveExchange')
            ->willThrowException(new Exception());
        /** @var Exchange $exchange */
        $exchange = $this->exchange;
        $exchange->setDbConnection($dbConnection);

        $this->assertEquals($exchange->save($exchange), false);
    }

    /**
     * Reset instance of exchange for the next test
     */
    protected function tearDown()
    {
        $this->exchange = null;
    }
}