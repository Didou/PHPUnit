<?php

/**
 * Class Exchange
 *
 * @author              Didier Youn <didier.youn@gmail.com>
 * @copyright           Copyright (c) 2016
 * @license             http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link                http://www.didier-youn.com
 */
class Exchange
{
    /** @var DatabaseConnection $dbConnection */
    private $dbConnection;
    /** @var EmailSender $emailSender */
    private $emailSender;
    /** @var Product $product */
    private $product;
    /** @var Receiver $receiver */
    private $receiver;
    /** @var string $dateDebut */
    private $dateDebut;
    /** @var string $dateFin */
    private $dateFin;

    /**
     * Exchange constructor.
     *
     * @param Receiver           $receiver
     * @param Product            $product
     * @param EmailSender        $emailSender
     * @param string             $dateDebut
     * @param string             $dateFin
     * @param DatabaseConnection $dbConnection
     */
    public function __construct(
        $receiver,
        $product,
        $emailSender,
        $dateDebut,
        $dateFin,
        $dbConnection)
    {
        $this->receiver = $receiver;
        $this->product = $product;
        $this->emailSender = $emailSender;
        $this->dateDebut = $dateDebut;
        $this->dateFin = $dateFin;
        $this->dbConnection = $dbConnection;
    }

    /**
     * Save Exchange data in database
     *
     * @param Exchange $exchange
     * @return bool
     */
    public function save(Exchange $exchange)
    {
        /** Check if we got an instance of Exchange class */
        if (!isset($exchange)) {
            return false;
        }
        /** @var string $dateDebut */
        $dateDebut = $exchange->dateDebut;
        /** @var string $dateFin */
        $dateFin = $exchange->dateFin;
        /** @var Product $product */
        $product = $this->getProduct();
        /** @var Receiver $receiver */
        $receiver = $this->getReceiver();
        /**
         * Several phases of tests :
         * --> if the dates are valid
         * --> if the product is valid
         * --> if the user is valid
         */
        if (!$this->_checkExchangeDate($dateDebut, $dateFin)
            || $product->isValid($product) === false
            || $receiver->isValid($receiver) === false
        ) {
            return false;
        }
        /** After check/valid data property, try to send mail to the receiver */
        if ($this->_checkReceiverAge($this->getReceiver())) {
            if ($this->getEmailSender()->sendEmail($this->getReceiver(), 'Le mail a bien été envoyé') === false) {
                return false;
            }
        } else {
            return false;
        }
        /** Last phase of the method, after the sending of mail we save an archive of the exchange in database */
        if ($this->getDbConnection()->saveExchange($exchange) === false) {
            return false;
        }

        return true;
    }

    /**
     * Check date availability
     *
     * @param string $dateDebut
     * @param string $dateFin
     * @return bool
     * @throws Exception $e
     */
    protected function _checkExchangeDate($dateDebut, $dateFin)
    {
        try {
            /** @var DateTime $dateDebut */
            $dateDebut = new DateTime($dateDebut);
            /** @var DateTime $dateDebut */
            $dateFin = new DateTime($dateFin);
            if ($dateDebut <= $dateFin) {
                return true;
            }
            return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param Receiver $receiver
     * @return bool
     */
    protected function _checkReceiverAge(Receiver $receiver)
    {
        if(isset($receiver) && (int)$receiver->getAge() > 18) {
            return true;
        }
        return false;
    }

    /**
     * @param string $dateDebut
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;
    }

    /**
     * @param string $dateFin
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;
    }

    /**
     * @param DatabaseConnection $dbConnection
     */
    public function setDbConnection($dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }

    /**
     * @param EmailSender $emailSender
     */
    public function setEmailSender($emailSender)
    {
        $this->emailSender = $emailSender;
    }

    /**
     * @param Product $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @param Receiver $receiver
     */
    public function setReceiver($receiver)
    {
        $this->receiver = $receiver;
    }

    /**
     * @return Receiver
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return DatabaseConnection
     */
    public function getDbConnection()
    {
        return $this->dbConnection;
    }

    /**
     * @return EmailSender
     */
    public function getEmailSender()
    {
        return $this->emailSender;
    }
}